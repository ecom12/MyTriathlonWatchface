/*

NOTES:
- Background: registerForActivityCompletedEvent()
- PersistedContent.getCourses()

Icons:
https://www.shutterstock.com/image-vector/footstep-icon-step-walk-footprint-mark-1402374770
*/

import Toybox.Graphics;
import Toybox.Lang;
import Toybox.Math;
import Toybox.System;
import Toybox.Time;
import Toybox.Time.Gregorian;
import Toybox.WatchUi;
using  Toybox.Background;
using Toybox.ActivityMonitor;
using Toybox.Complications as Complications;


typedef PointCoord as Array<Lang.Number>;
typedef RectCoord  as Array<PointCoord>;

//! This implements an analog watch face
//! Original design by Austen Harbour
class AnalogView extends WatchUi.WatchFace {
    private var _offscreenBuffer        as BufferedBitmap?;
    private var _staticBackgroundBuffer as BufferedBitmap?;
    private var _mainHandsBuffer        as BufferedBitmap?;
    private var _screenCenterPoint      as Array<Number>?;
    private var _fullScreenRefresh      as Boolean;
    private var _partialUpdatesAllowed  as Boolean;
    private var _powerBudgetExceeded    as Boolean;
    private var _drawHeartRateBitmap    as Boolean;
    private var _hrBufferCoord          as RectCoord = [[0,0],[999,999]];
    private var _heartRateUpdated    as Boolean = false;
    private var _onUpdateTotalCount as Number;
    private var _onUpdateMinCount   as Number;
    private var _bufferCachedCount  as Number;

    private var _isAwake as Boolean?;

    private var _hideHandsCountDown as Number  = 0;

    private var _topDataFont = Graphics.FONT_TINY;
    private var _arcColor1   = Graphics.COLOR_RED;
    private var _arcColor2   = Graphics.COLOR_DK_GRAY;

    enum Icons {
        ICN_BATTERY     = 0,
        ICN_BATTERY_100 = 1,
        ICN_BATTERY_80  = 2,
        ICN_BATTERY_60  = 3,
        ICN_BATTERY_40  = 4,
        ICN_BATTERY_20  = 5,
        ICN_BATTERY_10  = 6,
        ICN_RUNNING,
        ICN_CYCLING,
        ICN_SWIMMING,
        ICN_HEART,
        ICN_AIRPLANE,
        ICN_BT,
        ICN_NO_BT,
        ICN_STEPS,
        ICN_PHONE,
        ICN_NO_PHONE,
        ICN_DANGER,

        ICN_COUNT
    }
    private var _icons as Array<BitmapResource> = new [ICN_COUNT];

    private var _newHeartRate as Number;
    private var _steps        as Number;

    private var _scrnH as Number = 0;
    private var _scrnW as Number = 0;

    private var _boundingBoxes as Array<RectCoord> = [];

    private const BOX_ICONS = 0;
    private const BOX_BAT   = 1;
    private const BOX_DATE  = 2;
    private const BOX_HR    = 3;
    private const BOX_STEPS = 4;


    //! Initialize variables for this view
    public function initialize() {
        WatchFace.initialize();
        _fullScreenRefresh = true;
        _partialUpdatesAllowed = (WatchUi.WatchFace has :onPartialUpdate);
        _powerBudgetExceeded   = false;
        _drawHeartRateBitmap   = true;

        _onUpdateMinCount   = 0;
        _onUpdateTotalCount = 0;
        _bufferCachedCount  = 0;

        _newHeartRate    = 0;
        _heartRateUpdated = false;
        _steps           = 0;

        System.println("[NFO] AnalogView.initialize() done!");
    }


    function defineBoundingBoxes(dc) {
        // "bounds" format is an array as follows [  [x1,y1] , [x2,y2] ]
        //
        //   [x1,y1] --------------+
        //      |                  |
        //      |                  |
        //      +---------------[x2,y2]
        //

        var hourH       = 32 as Lang.Number;
        var txtBoxH     = ( ((_scrnH/2)-hourH) / 3).toNumber();

        var halfW       = (_scrnW / 2).toNumber();
        var l1HalfWidth = (_scrnW * 0.20).toNumber();
        var l2Witdh     = (_scrnW * 0.35).toNumber();
        var l3Width     = (_scrnW * 0.42).toNumber();

        // Top box
        var bbox_top  = [ [ halfW - l1HalfWidth, hourH                 ] as PointCoord,
                          [ halfW + l1HalfWidth, hourH + txtBoxH       ] as PointCoord ] as RectCoord;

        // middle left
        var bbox_ml   = [ [ halfW - l2Witdh , hourH + txtBoxH          ] as PointCoord,
                          [ halfW           , hourH + (txtBoxH * 2)    ] as PointCoord] as RectCoord;

        // middle right
        var bbox_mr   = [ [ halfW           , hourH + txtBoxH          ] as PointCoord,
                          [ halfW + l2Witdh , hourH + (txtBoxH * 2)    ] as PointCoord] as RectCoord;

        // bottom left
        var bbox_btl  = [ [ halfW - l3Width , hourH + (txtBoxH * 2)    ] as PointCoord,
                          [ halfW           , hourH + (txtBoxH * 3)    ] as PointCoord] as RectCoord;

        // bottom right
        var bbox_btr  = [ [ halfW           , hourH + (txtBoxH * 2)    ] as PointCoord,
                          [ halfW + l3Width , hourH + (txtBoxH * 3)    ] as PointCoord] as RectCoord;

        _boundingBoxes = [
                bbox_top,
                bbox_ml,
                bbox_mr,
                bbox_btl,
                bbox_btr
        ] as Array<RectCoord>;

        var x1 = _boundingBoxes[BOX_HR][0][0] + 8 + _icons[ICN_HEART].getWidth() + 6;
        var y1 = _boundingBoxes[BOX_HR][0][1] + 2;
        _hrBufferCoord = [  [ x1     , y1      ],
                            [ x1 + 40, y1 + 28 ]  ];

    }

    private function loadResources() {
        // Load the custom font we use for drawing the 3, 6, 9, and 12 on the watchface.
        //        _font = WatchUi.loadResource($.Rez.Fonts.id_font_black_diamond) as FontResource;

        // Load icons
        _icons[ICN_BATTERY]     = WatchUi.loadResource($.Rez.Drawables.BatteryIcon)    as BitmapResource;
        _icons[ICN_BATTERY_100] = WatchUi.loadResource($.Rez.Drawables.Battery100Icon) as BitmapResource;
        _icons[ICN_BATTERY_80]  = WatchUi.loadResource($.Rez.Drawables.Battery80Icon)  as BitmapResource;
        _icons[ICN_BATTERY_60]  = WatchUi.loadResource($.Rez.Drawables.Battery60Icon)  as BitmapResource;
        _icons[ICN_BATTERY_40]  = WatchUi.loadResource($.Rez.Drawables.Battery40Icon)  as BitmapResource;
        _icons[ICN_BATTERY_20]  = WatchUi.loadResource($.Rez.Drawables.Battery20Icon)  as BitmapResource;
        _icons[ICN_BATTERY_10]  = WatchUi.loadResource($.Rez.Drawables.Battery10Icon)  as BitmapResource;
        _icons[ICN_HEART]       = WatchUi.loadResource($.Rez.Drawables.HeartIcon)      as BitmapResource;
        _icons[ICN_AIRPLANE]    = WatchUi.loadResource($.Rez.Drawables.PlaneIcon)      as BitmapResource;
        _icons[ICN_BT]          = WatchUi.loadResource($.Rez.Drawables.BTIcon)         as BitmapResource;
        _icons[ICN_NO_BT]       = WatchUi.loadResource($.Rez.Drawables.NoBTIcon)       as BitmapResource;
        _icons[ICN_STEPS]       = WatchUi.loadResource($.Rez.Drawables.StepsIcon)      as BitmapResource;
        _icons[ICN_RUNNING]     = WatchUi.loadResource($.Rez.Drawables.RunningIcon)    as BitmapResource;
        _icons[ICN_CYCLING]     = WatchUi.loadResource($.Rez.Drawables.CyclingIcon)    as BitmapResource;
        _icons[ICN_SWIMMING]    = WatchUi.loadResource($.Rez.Drawables.SwimmingIcon)   as BitmapResource;
        _icons[ICN_PHONE]       = WatchUi.loadResource($.Rez.Drawables.PhoneIcon)      as BitmapResource;
        _icons[ICN_NO_PHONE]    = WatchUi.loadResource($.Rez.Drawables.NoPhoneIcon)    as BitmapResource;
        _icons[ICN_DANGER]      = WatchUi.loadResource($.Rez.Drawables.DangerIcon)     as BitmapResource;


    }

    public function hideHands() as Void {
        if (_hideHandsCountDown > 0) {
            _hideHandsCountDown = 0;
        } else {
            _hideHandsCountDown = 12;
        }
        if (_isAwake == false) {
            WatchUi.requestUpdate();
        }
    }

    //! Configure the layout of the watchface for this device
    //! @param dc Device context
    public function onLayout(dc as Dc) as Void {
        _scrnH = dc.getHeight();
        _scrnW = dc.getWidth();
    
        loadResources();
        defineBoundingBoxes(dc);

        if (Graphics has :createBufferedBitmap) {
            // If this device supports createBufferedBitmap, allocate the buffers we use for drawing
            // Allocate a full screen size buffer with a palette of only 4 colors to draw
            // the background image of the watchface.  This is used to facilitate blanking
            // the second hand during partial updates of the display
            _offscreenBuffer = Graphics.createBufferedBitmap({
                :width  => _scrnH,
                :height => _scrnW/*,
                :palette=> [
                    Graphics.COLOR_DK_GRAY,
                    Graphics.COLOR_LT_GRAY,
                    Graphics.COLOR_BLACK,
                    Graphics.COLOR_WHITE,
                    Graphics.COLOR_RED
                ] as Array<ColorValue>*/
            }).get();

            // I really don't understand how to use the palette.
            // I can't have the correct render even if I set all colors used.
            // Even worse: the order of colors in the palette does impact the output!
            // Documentation is poor and does not help understanding how to use the palette feature.
            _staticBackgroundBuffer = Graphics.createBufferedBitmap({
                :width  => _scrnH,
                :height => _scrnW/*,
                :palette=> [
                    Graphics.COLOR_BLACK,
                    Graphics.COLOR_WHITE,
                    Graphics.COLOR_RED,
                    Graphics.COLOR_DK_RED
                ] as Array<ColorValue>*/
            }).get();

            _mainHandsBuffer = Graphics.createBufferedBitmap({
                :width  => _scrnH,
                :height => _scrnW/*,
                :palette=> [
                    Graphics.COLOR_BLACK,
                    Graphics.COLOR_WHITE,
                    Graphics.COLOR_RED,
                    Graphics.COLOR_DK_RED
                ] as Array<ColorValue>*/
            }).get();

            drawStaticBackground(_staticBackgroundBuffer.getDc());

        } else {
            _offscreenBuffer        = null;
            _staticBackgroundBuffer = null;
            _mainHandsBuffer        = null;
        }

        _screenCenterPoint = [dc.getWidth() / 2, dc.getHeight() / 2] as Array<Number>;
    }

    //! This function is used to generate the coordinates of the 4 corners of the polygon
    //! used to draw a watch hand. The coordinates are generated with specified length,
    //! tail length, and width and rotated around the center point at the provided angle.
    //! 0 degrees is at the 12 o'clock position, and increases in the clockwise direction.
    //! @param centerPoint The center of the clock
    //! @param angle Angle of the hand in radians
    //! @param handLength The length of the hand from the center to point
    //! @param tailLength The length of the tail of the hand
    //! @param width The width of the watch hand
    //! @return The coordinates of the watch hand
    private function generateHandCoordinates(centerPoint as Array<Number>, angle as Float, handLength as Number, tailLength as Number, width as Number) as Array< Array<Float> > {
        // Map out the coordinates of the watch hand
        var coords = [[-(width / 2), tailLength] as Array<Number>,
                      [-(width / 2), -handLength] as Array<Number>,
                      [0, -handLength - 7] as Array<Number>,
                      [width / 2, -handLength] as Array<Number>,
                      [width / 2, tailLength] as Array<Number>] as Array< Array<Number> >;
        var result = new Array< Array<Float> >[coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i++) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + 0.5;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + 0.5;

            result[i] = [centerPoint[0] + x, centerPoint[1] + y] as Array<Float>;
        }

        return result;
    }

    //! Draws the clock tick marks around the outside edges of the screen.
    //! @param dc Device context
    private function drawHourLabels(dc as Dc) as Void {
        var width = dc.getWidth();
        var height = dc.getHeight();

        // Draw the 3, 6, 9, and 12 hour labels.
        var font = Graphics.FONT_XTINY;
        if (font != null) {
            var outerRad = width / 2 - 10;

            var angtest = Math.PI / 6;
            var dx = outerRad * Math.cos(angtest);
            var dy = outerRad * Math.sin(angtest);
            var textJustification = Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER;

            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);

            dc.drawText(width/2 + dy, height/2 - dx , font, "1", textJustification);
            dc.drawText(width/2 + dx, height/2 - dy , font, "2", textJustification);
            dc.drawText(width/2 + dx , height/2 + dy, font, "4", textJustification);
            dc.drawText(width/2 + dy, height/2 + dx , font, "5", textJustification);
            dc.drawText(width/2 - dy, height/2 + dx , font, "7", textJustification);
            dc.drawText(width/2 - dx, height/2 + dy , font, "8", textJustification);
            dc.drawText(width/2 - dx, height/2 - dy , font, "10", textJustification);
            dc.drawText(width/2 - dy, height/2 - dx , font, "11", textJustification);

            font = Graphics.FONT_MEDIUM;
            dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_BLACK);

            dc.drawText(width / 2, (height / 2) - outerRad + 6, font, "12", textJustification);
            dc.drawText(width / 2 + outerRad, (height / 2) - 6, font, "3", textJustification);

            dc.drawText(width / 2, height / 2 + outerRad - 6, font, "6", textJustification);
            dc.drawText(width / 2 - outerRad, (height / 2) + 6, font, "9", textJustification);

            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        }
   }


    private function drawHashMarks(dc as Dc) as Void {
        var x1, x2, y1, y2;

        var outerRad = dc.getWidth() / 2;
        var innerRad = outerRad - 10;

        // 1 min angle
        var ang1min = Math.PI/30;

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);

        for (var i=0; i<7; i++) {
            // Precalculate coordinates
            // skip 5 min because hour label will be disblayed here
            if (i!=4) {
                x1 = outerRad * Math.cos(ang1min * (i+1));
                x2 = innerRad * Math.cos(ang1min * (i+1));
                y1 = outerRad * Math.sin(ang1min * (i+1));
                y2 = innerRad * Math.sin(ang1min * (i+1));

                // Now we can draw the marks
                // 8min -> 22min
                dc.drawLine( outerRad+x1, outerRad-y1, outerRad+x2, outerRad-y2 );
                dc.drawLine( outerRad+x1, outerRad+y1, outerRad+x2, outerRad+y2 );

                // 38min -> 52min
                dc.drawLine( outerRad-x1, outerRad-y1, outerRad-x2, outerRad-y2 );
                dc.drawLine( outerRad-x1, outerRad+y1, outerRad-x2, outerRad+y2 );

                // And now we rotate
                dc.drawLine( outerRad+y1, outerRad-x1, outerRad+y2, outerRad-x2 );
                dc.drawLine( outerRad+y1, outerRad+x1, outerRad+y2, outerRad+x2 );
                dc.drawLine( outerRad-y1, outerRad-x1, outerRad-y2, outerRad-x2 );
                dc.drawLine( outerRad-y1, outerRad+x1, outerRad-y2, outerRad+x2 );
            }
        }
    }


    // - angle in degree
    // - option: :drawAll or :drawData or :drawBitmap
    private function drawSportStat(dc as Dc, activity as Number, angle as Number, iconId as Number, option) {
        var width = dc.getWidth();
        var height = dc.getHeight();

        // rad: radius of the distance arc
        var rad        = 34;

        var centerRad  = (4 * height / 5) - (height / 2);
        var x          = width / 2  + centerRad * Math.cos(Math.toRadians(angle));
        var y          = height / 2 - centerRad * Math.sin(Math.toRadians(angle));

        if (option==:drawAll || option==:drawBitmap) {
            var xIcn = x - _icons[iconId].getWidth()/2;
            var yIcn = y - rad + 8;
            dc.drawBitmap(xIcn, yIcn, _icons[iconId]);
        }

        if (option==:drawAll || option==:drawData) {
            // arcOpenHalfAngle: part of the arc which is not closed (half of it)
            var arcOpenHalfAngle = 45;
            var dist       = 0.0f;
            var prevDist   = 0.0f;
            if ($.statistics[activity]!=null) {
                dist     = $.statistics[activity].weeklyDistance     / 1000.0f;
                prevDist = $.statistics[activity].prevWeeklyDistance / 1000.0f;
            } else {

            }

            var dataString = dist.format("%.1f") + "km";
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
            dc.drawText(x, y-2, Graphics.FONT_XTINY, dataString, Graphics.TEXT_JUSTIFY_CENTER);
            dc.setPenWidth(4);

            if (prevDist == 0.0f) {
                if (dist > 0.0f) {
                    // 100%
                    dc.setColor(_arcColor1, Graphics.COLOR_TRANSPARENT);
                } else {
                    // 0%
                    dc.setColor(_arcColor2, Graphics.COLOR_TRANSPARENT);
                }
                dc.drawArc(x, y, rad, Graphics.ARC_CLOCKWISE, angle-arcOpenHalfAngle, angle+arcOpenHalfAngle);
            } else if (dist >= prevDist) {
                // More than last week -> full red arc
                dc.setColor(_arcColor1, Graphics.COLOR_TRANSPARENT);
                dc.drawArc(x, y, rad, Graphics.ARC_CLOCKWISE, angle-arcOpenHalfAngle, angle+arcOpenHalfAngle);
            } else {
                var arcAngle = 360 - (2*arcOpenHalfAngle);
                var arcSplit = 0;
                arcSplit = dist * arcAngle / prevDist;
                if (dist > 0.0f) {
                    dc.setColor(_arcColor1, Graphics.COLOR_TRANSPARENT);
                    dc.drawArc(x, y, rad, Graphics.ARC_CLOCKWISE, angle-arcOpenHalfAngle, angle-arcOpenHalfAngle-arcSplit);
                }
                dc.setColor(_arcColor2, Graphics.COLOR_TRANSPARENT);
                dc.drawArc(x, y, rad, Graphics.ARC_CLOCKWISE, angle-arcOpenHalfAngle-arcSplit, angle+arcOpenHalfAngle);
            }
        }
    }

    private function drawSportsStats(dc as Dc, option) as Void {
        drawSportStat(dc, Activity.SPORT_SWIMMING, 210, ICN_SWIMMING, option);
        drawSportStat(dc, Activity.SPORT_CYCLING , 270, ICN_CYCLING , option);
        drawSportStat(dc, Activity.SPORT_RUNNING , 330, ICN_RUNNING , option);

    }

    public function drawHoursMinutesHands_simple(dc as Dc, clockTime) as Void {
        var width = dc.getWidth();
        var height = dc.getHeight();
    
        // Use white to draw the hour and minute hands
        dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_TRANSPARENT);

        if (_screenCenterPoint != null) {
            // Draw the hour hand. Convert it to minutes and compute the angle.
            var hourHandAngle = (((clockTime.hour % 12) * 60) + clockTime.min);
            hourHandAngle = hourHandAngle / (12 * 60.0);
            hourHandAngle = hourHandAngle * Math.PI * 2;
            dc.fillPolygon(generateHandCoordinates(_screenCenterPoint, hourHandAngle, 60, 0, 8));
        }

        if (_screenCenterPoint != null) {
            // Draw the minute hand.
            var minuteHandAngle = (clockTime.min / 60.0) * Math.PI * 2;
            dc.fillPolygon(generateHandCoordinates(_screenCenterPoint, minuteHandAngle, 90, 0, 7));
        }

        // Draw the arbor in the center of the screen.
        dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_BLACK);
        dc.fillCircle(width / 2, height / 2, 7);
        dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_BLACK);
        dc.drawCircle(width / 2, height / 2, 7);
    }

    public function drawHoursMinutesHands_new(dc as Dc, clockTime) as Void {
        var width = dc.getWidth();
        var height = dc.getHeight();
    
        // Use white to draw the hour and minute hands
    
        if (_screenCenterPoint != null) {
            // Draw the minute hand.
            var minuteHandAngle = (clockTime.min / 60.0) * Math.PI * 2;
            dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
            var coord = generateHandCoordinates(_screenCenterPoint, minuteHandAngle, 105, 0, 15);
            dc.fillPolygon(coord);
            dc.setPenWidth(1);
            dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
            for (var i=1; i< coord.size(); i++) {
                dc.drawLine(coord[i-1][0], coord[i-1][1],
                            coord[i][0], coord[i][1]);
            }
            var x1, y1;
            var x2, y2;
            var cos = Math.cos(minuteHandAngle);
            var sin = Math.sin(minuteHandAngle);
            y1 = _scrnH/2 - ( 95 * cos );
            y2 = _scrnH/2 - ( 30 * cos );
            x1 = _scrnW/2 + ( 95 * sin );
            x2 = _scrnW/2 + ( 30 * sin );
            
            dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_TRANSPARENT);
            dc.setPenWidth(4);
            dc.drawLine(x1, y1, x2, y2);

            // Draw the hour hand. Convert it to minutes and compute the angle.
            var hourHandAngle = (((clockTime.hour % 12) * 60) + clockTime.min);
            hourHandAngle = hourHandAngle / (12 * 60.0);
            hourHandAngle = hourHandAngle * Math.PI * 2;
            dc.setColor(Graphics.COLOR_LT_GRAY, Graphics.COLOR_TRANSPARENT);
            coord = generateHandCoordinates(_screenCenterPoint, hourHandAngle, 70, 0, 15);
            dc.fillPolygon(coord);
            dc.setPenWidth(1);
            dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
            for (var i=1; i< coord.size(); i++) {
                dc.drawLine(coord[i-1][0], coord[i-1][1],
                            coord[i][0], coord[i][1]);
            }

            cos = Math.cos(hourHandAngle);
            sin = Math.sin(hourHandAngle);
            y1 = _scrnH/2 - ( 60 * cos );
            y2 = _scrnH/2 - ( 30 * cos );
            x1 = _scrnW/2 + ( 60 * sin );
            x2 = _scrnW/2 + ( 30 * sin );

            dc.setColor(Graphics.COLOR_ORANGE, Graphics.COLOR_TRANSPARENT);
            dc.setPenWidth(4);
            dc.drawLine(x1, y1, x2, y2);
        }

        // Draw the arbor in the center of the screen.
        dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_BLACK);
        dc.fillCircle(width / 2, height / 2, 7);

        dc.setPenWidth(2);
        dc.setColor(Graphics.COLOR_BLACK,Graphics.COLOR_BLACK);
        dc.drawCircle(width / 2, height / 2, 3);
    }

    public function drawSteps(dc as Dc) as Void {
        var dataString;
        var y1 = _boundingBoxes[BOX_STEPS][0][1];
        var x2 = _boundingBoxes[BOX_STEPS][1][0] - _icons[ICN_STEPS].getWidth();
        dc.drawBitmap(x2 , y1+3, _icons[ICN_STEPS]);
        if (_steps != 0) {
            dataString = _steps.toString();
        } else  {
            dataString = "--";
        }
        dc.drawText(x2 - 4, y1, _topDataFont, dataString, Graphics.TEXT_JUSTIFY_RIGHT);
    }

    public function drawDanger(dc as Dc) as Void {
        var x = (_scrnW - _icons[ICN_DANGER].getWidth()) / 2;
        var y = _scrnH/2 - _icons[ICN_DANGER].getHeight() - 20;
        dc.drawBitmap(x, y, _icons[ICN_DANGER]);
    }

    public function drawBatteryStatus(dc as Dc) {
        var xoffset = 10;
        var x1 = _boundingBoxes[BOX_BAT][0][0] + xoffset;
        var y1 = _boundingBoxes[BOX_BAT][0][1];
        var batt = (System.getSystemStats().battery + 0.5).toNumber();
        dc.setPenWidth(1);
        dc.drawLine     (x1+3, y1+2, x1+8, y1+2);
        dc.drawLine     (x1+3, y1+3, x1+8, y1+3);
        dc.drawRectangle(x1 , y1+5, 12, 20);
        if (batt>20) {
            dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_TRANSPARENT);
        } else if (batt>10) {
            dc.setColor(Graphics.COLOR_ORANGE, Graphics.COLOR_TRANSPARENT);
        } else {
            dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
        }
        var dx=(batt*16/100).toNumber();
        if (dx==0) {dx=1;}
        dc.fillRectangle(x1+2, y1+7+16-dx, 8, dx);

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        var dataString = (System.getSystemStats().battery + 0.5).toNumber().toString() + "%";
        dc.drawText(x1 + 18, y1, _topDataFont, dataString, Graphics.TEXT_JUSTIFY_LEFT);
    }

    public function drawHeartRate(dc as Dc, option) as Void {
        if (option==:drawAll || option==:drawBitmap) {
            var x1 = _boundingBoxes[BOX_HR][0][0] + 8;
            var y1 = _boundingBoxes[BOX_HR][0][1];
            dc.drawBitmap(x1 , y1+3, _icons[ICN_HEART]);    
        }

        if (option==:drawAll || option==:drawData) {
            var dataString;
            if (_newHeartRate != 0) {
                dataString = _newHeartRate.toString();
                if (_newHeartRate < 100) {
                    // Add one space to ensure the string has always 3 characters: no need to clead dc before drawing
                    dataString = dataString + "  ";
                }
            } else  {
                dataString = "--  ";
            }
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
            dc.drawText(_hrBufferCoord[0][0], _hrBufferCoord[0][1], _topDataFont, dataString, Graphics.TEXT_JUSTIFY_LEFT);

            // When called from complication update while in low power mode, this is to indicate
            // that HR has been updated in the screen buffer and that it must be redrawn
            _drawHeartRateBitmap = true;
        }
    }

    public function drawStatusIcons(dc as Dc) as Void {
        // Notes:
        //  * Not possible to get Airplane mode status
        //    -> https://forums.garmin.com/developer/connect-iq/f/discussion/311470/how-to-query-for-airplane-mode-status

        var x, y;
        x = _scrnW/2 - 24;
        y = _boundingBoxes[BOX_ICONS][0][1];
        if (System.getDeviceSettings().phoneConnected) {
            dc.drawBitmap(x, y, _icons[ICN_PHONE]);
            x = x + _icons[ICN_PHONE].getWidth();
        } else {
            dc.drawBitmap(x, y, _icons[ICN_NO_PHONE]);
            x = x + _icons[ICN_NO_PHONE].getWidth();
        }
    
        var btInfo = System.getDeviceSettings().connectionInfo[:bluetooth];
        if (btInfo!=null) {
            if (btInfo.state == System.CONNECTION_STATE_CONNECTED) {
                dc.drawBitmap(x, y, _icons[ICN_BT]);
            } else {
                dc.drawBitmap(x, y, _icons[ICN_NO_BT]);
            }
        }
    }

    // For debug purposes
    public function drawPattern(dc as Dc) as Void {
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        for (var i = 0 as Lang.Number; i < _boundingBoxes.size(); i++) {
            var x1 = _boundingBoxes[i][0][0];
            var y1 = _boundingBoxes[i][0][1];
            var x2 = _boundingBoxes[i][1][0];
            var y2 = _boundingBoxes[i][1][1];
            dc.drawRectangle(x1, y1, x2-x1, y2-y1);
        }
    }

    public function drawDateString(dc as Dc) as Void {
        var info = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
        var dateStr = Lang.format("$1$ $2$", [info.day_of_week, info.day]);

        var x1 = _boundingBoxes[BOX_DATE][0][0];
        var y1 = _boundingBoxes[BOX_DATE][0][1];

        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(x1 + 20, y1, _topDataFont, dateStr, Graphics.TEXT_JUSTIFY_LEFT);
    }

    public function drawHideHandsCountDown(dc as Dc) as Void {
        var datastring = _hideHandsCountDown.toString();

        dc.setColor(Graphics.COLOR_DK_BLUE, Graphics.COLOR_DK_BLUE);
        dc.fillCircle(_scrnW/2, _scrnH/2, 25);
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        dc.drawText(_scrnW/2, _scrnH/2, _topDataFont, datastring, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }

    public function drawStaticBackground(dc as Dc) {
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        drawHeartRate  (dc, :drawBitmap);
        drawHashMarks  (dc);
        drawHourLabels (dc);
        drawSportsStats(dc, :drawBitmap);
//        drawPattern    (dc);
    }

    public function drawData(dc as Dc) as Void {
        // Step colors for text data
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        drawStatusIcons  (dc);
        drawDateString   (dc);
        drawBatteryStatus(dc);
        drawHeartRate    (dc, :drawData);
        drawSteps        (dc);
        drawSportsStats  (dc, :drawData);
    }

    //! Handle the update event
    //! @param dc Device context
    public function onUpdate(dc as Dc) as Void {
        var targetDc    = null;
        var clockTime   = System.getClockTime();
        var isBufferCached = false;
    
        _onUpdateTotalCount++;
        if (clockTime.sec==0) {
            _onUpdateMinCount++;

            // Every minute, clear HR value if it has not been refreshed during the last minute
            if (_isAwake == false) {
                if ( _heartRateUpdated == false ) {
                    _newHeartRate = 0;
                }
                _heartRateUpdated = false;
            }
        }

        // We always want to refresh the full screen when we get a regular onUpdate call.
        _fullScreenRefresh = true;
        if (null != _offscreenBuffer) {
            // If we have an offscreen buffer that we are using to draw the background,
            // set the draw context of that buffer as our target.
            targetDc       = _offscreenBuffer.getDc();
            isBufferCached = _offscreenBuffer.isCached();
            if (isBufferCached) { _bufferCachedCount++; }
            dc.clearClip();
        } else {
            targetDc = dc;
        }

        // Fill the entire background with Black.
        targetDc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        targetDc.clear();

        // Draw the background, then add the data
        targetDc.drawBitmap(0, 0, _staticBackgroundBuffer);
        drawData             (targetDc);

        if (_powerBudgetExceeded==true) {
            drawDanger(targetDc);
        }

        // Draw background, and then hour/minutes hands if not hidden
        dc.drawBitmap(0, 0, _offscreenBuffer);
        if (_hideHandsCountDown==0) {
            var handsDc = _mainHandsBuffer.getDc();
            handsDc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
            handsDc.clear();
            drawHoursMinutesHands_new(handsDc, clockTime);
            dc.drawBitmap(0, 0, _mainHandsBuffer);
        }

// CPO: for now, forcing the call to onPartialupdate() from onUpdate().
// This is not optimized, will need rework later on.
_partialUpdatesAllowed = true;
        if (_partialUpdatesAllowed) {
            // If this device supports partial updates and they are currently
            // allowed run the onPartialUpdate method to draw the second hand.
            onPartialUpdate(dc);
        } else if (_isAwake) {
// CPO: this part should be updated
            // Otherwise, if we are out of sleep mode, draw the second hand
            // directly in the full update method.
            dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
            var secondHand = (clockTime.sec / 60.0) * Math.PI * 2;

            if (_screenCenterPoint != null) {
                dc.fillPolygon(generateHandCoordinates(_screenCenterPoint, secondHand, 60, 20, 2));
            }
            if (_hideHandsCountDown>0) {
                _hideHandsCountDown--;
            }
        }

        _fullScreenRefresh = false;
    }

 

    //! Handle the partial update event
    //! @param dc Device context
    public function onPartialUpdate(dc as Dc) as Void {
        if (_hideHandsCountDown>0) {
            _hideHandsCountDown--;
            drawHideHandsCountDown(dc);
            return;
        }
        // If we're not doing a full screen refresh we need to re-draw the background
        // before drawing the updated second hand position. Note this will only re-draw
        // the background in the area specified by the previously computed clipping region.
        if (!_fullScreenRefresh) {
            dc.drawBitmap(0, 0, _offscreenBuffer);
            dc.drawBitmap(0, 0, _mainHandsBuffer);
        }

        var clockTime = System.getClockTime();

        if (_screenCenterPoint != null) {
            var secondHandPoints = generateSecondHandLine(clockTime.sec, 100, 20);

            // Update the clipping rectangle to the new location of the second hand.
            var curClip    = getBoundingBox(secondHandPoints);

            // If heart rate has been updated it should be included in the clipping region
            if (_drawHeartRateBitmap == true)
            {
                _drawHeartRateBitmap = false;

                var x1 = _hrBufferCoord[0][0];
                var y1 = _hrBufferCoord[0][1];
                var x2 = _hrBufferCoord[1][0];
                var y2 = _hrBufferCoord[1][1];

                if (x1<curClip[0][0]) {  curClip[0][0] = x1; }
                if (y1<curClip[0][1]) {  curClip[0][1] = y1; }
                if (x2>curClip[1][0]) {  curClip[1][0] = x2; }
                if (y2>curClip[1][1]) {  curClip[1][1] = y1; }
            }
            var bBoxWidth  = curClip[1][0] - curClip[0][0] + 4;
            var bBoxHeight = curClip[1][1] - curClip[0][1] + 4;
            dc.setClip(curClip[0][0] - 2, curClip[0][1] - 2, bBoxWidth, bBoxHeight);

            // Debug code to draw the clipping region
/*
            dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLUE);
            dc.drawRectangle(curClip[0][0], curClip[0][1], bBoxWidth, bBoxHeight);
*/
            dc.setPenWidth(3);
            dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_TRANSPARENT);
            // Change a bit the second hand to have a visual indication of active or low power mode
            if (_fullScreenRefresh == false) {
                // Draw the second hand to the screen.
                dc.drawLine(secondHandPoints[0][0], secondHandPoints[0][1], secondHandPoints[1][0], secondHandPoints[1][1]);
            } else {
                secondHandPoints = generateSecondHandLine(clockTime.sec, 100, -30);
                dc.drawLine(secondHandPoints[0][0], secondHandPoints[0][1], secondHandPoints[1][0], secondHandPoints[1][1]);

                dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_TRANSPARENT);
                secondHandPoints = generateSecondHandLine(clockTime.sec, 30, 20);
                dc.drawLine(secondHandPoints[0][0], secondHandPoints[0][1], secondHandPoints[1][0], secondHandPoints[1][1]);
            }

        }
/*
        // This is to force HR update every second while in low power mode
        if (_newHeartRate>105) {
            xxx = -1;
        } else if (_newHeartRate<95) {
            xxx = 1;
        }
        setHR(_newHeartRate + xxx);
*/
    }
//var xxx = 1;

    private function generateSecondHandLine(seconds as Number, handLength as Number, tailLength as Number) as Array< Array<Number> > {
        var x1, y1;
        var x2, y2;
        var angle = (seconds / 60.0) * Math.PI * 2;
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        y1 = (_scrnH/2 - ( handLength * cos )) as Number;
        y2 = (_scrnH/2 + ( tailLength * cos )) as Number;

        x1 = (_scrnW/2 + ( handLength * sin )) as Number;
        x2 = (_scrnW/2 - ( tailLength * sin )) as Number;

        var result = [[x1.toNumber(), y1.toNumber()], [x2.toNumber(), y2.toNumber()]] as Array< Array<Lang.Number> > ;

        return result;
    }

    //! Compute a bounding box from the passed in points
    //! @param points Points to include in bounding box
    //! @return The bounding box points
    private function getBoundingBox(points as Array< Array<Number> >) as Array< Array<Number> > {
        var min = [9999, 9999] as Array<Number>;
        var max = [0,0] as Array<Number>;

        for (var i = 0; i < points.size(); ++i) {
            if (points[i][0] < min[0]) {
                min[0] = points[i][0];
            }

            if (points[i][1] < min[1]) {
                min[1] = points[i][1];
            }

            if (points[i][0] > max[0]) {
                max[0] = points[i][0];
            }

            if (points[i][1] > max[1]) {
                max[1] = points[i][1];
            }
        }

        return [min, max] as Array< Array<Number> >;
    }

    //! This method is called when the device re-enters sleep mode.
    //! Set the isAwake flag to let onUpdate know it should stop rendering the second hand.
    public function onEnterSleep() as Void {
        _isAwake = false;
        WatchUi.requestUpdate();
    }

    //! This method is called when the device exits sleep mode.
    //! Set the isAwake flag to let onUpdate know it should render the second hand.
    public function onExitSleep() as Void {
        _isAwake = true;
    }

    //! Turn off partial updates
    public function turnPartialUpdatesOff() as Void {
        _partialUpdatesAllowed = false;
    }

    public function notifyPowerBudgetExceeded() as Void {
        _powerBudgetExceeded=true;
    }

    function setHR(valueHR as Lang.Number) {
        _heartRateUpdated = true;

        if (valueHR == _newHeartRate) {
            return;
        }
        if (_offscreenBuffer==null) {
            return;
        }

        _newHeartRate = valueHR;
        drawHeartRate(_offscreenBuffer.getDc(), :drawData);
    }

    function setSteps(valueSteps as Lang.Number) {
        _steps = valueSteps;
    }
}

//! Receives watch face events
class AnalogDelegate extends WatchUi.WatchFaceDelegate {
    private var _view as AnalogView;

    //! Constructor
    //! @param view The analog view
    public function initialize(view as AnalogView) {
        WatchFaceDelegate.initialize();
        _view = view;
    }

    public function onPress(clickEvent as WatchUi.ClickEvent) as Lang.Boolean {
 /*       System.println("Click: " + clickEvent.getType());
        if (clickEvent.getType()==CLICK_TYPE_RELEASE) {
            System.println("RELEASE");
        }*/
        // Request to hide hands for 5 seconds
        _view.hideHands();
        return true;
    }

    //! The onPowerBudgetExceeded callback is called by the system if the
    //! onPartialUpdate method exceeds the allowed power budget. If this occurs,
    //! the system will stop invoking onPartialUpdate each second, so we notify the
    //! view here to let the rendering methods know they should not be rendering a
    //! second hand.
    //! @param powerInfo Information about the power budget
    public function onPowerBudgetExceeded(powerInfo as WatchFacePowerInfo) as Void {
        System.println("Average execution time: " + powerInfo.executionTimeAverage);
        System.println("Allowed execution time: " + powerInfo.executionTimeLimit);
        _view.notifyPowerBudgetExceeded();
        _view.turnPartialUpdatesOff();
    }
}


(:background)
class MyServiceDelegate extends System.ServiceDelegate {

    enum {
        NFO_NEW_WEEK = 0,
        NFO_ACTIVITY_COMPLETED
    }

    function initialize() {
        ServiceDelegate.initialize();
    }

    public function onTemporalEvent() as Void {
        Background.exit(NFO_NEW_WEEK);
    }

    function onActivityCompleted(activity)  {
        println("[NFO] MyServiceDelegate.onActivityCompleted()" + activity[0] + " - " + activity[1]);

        Background.exit(NFO_ACTIVITY_COMPLETED);
    }
}