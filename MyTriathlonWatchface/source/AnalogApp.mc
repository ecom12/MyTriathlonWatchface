import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
using  Toybox.Background;
import Toybox.System;
import Toybox.Complications;
using Toybox.Background;
using Toybox.Time;
using Toybox.Time.Gregorian;
import Toybox.Application.Storage;
using Toybox.Complications as Complications;

/*
 var complication = Complications.getComplication(
    new Id(Complications.COMPLICATION_TYPE_WEEKLY_RUN_DISTANCE)
);
System.println(complication.value);
*/

class ActivityStat {
    public function initialize(act) {
        if(act instanceof Number) {
            activity       = act;
            activityCount  = 0;
            weeklyDistance = 0;
            weeklyDuration = 0;
        } else {
            // Should be dictionary then
            restoreData( act );
        }
    }

    // this is intended to be called by order, from older activity to the latest one, in order to have a valid 'lastStart'
    // prevWeek: set to true if the data is for previous week.
    // distance: in meter
    // duration: in seconds
    public function addActivityData( prevWeek as Lang.Boolean, startDate as Lang.Number, distance as Lang.Number, duration as Lang.Number) {
        System.println("[NFO] ActivityStat.addActivityData( " + activity + " )");
        lastStart       = startDate;
        if (prevWeek==true) {
            prevActivityCount++;
            prevWeeklyDistance += distance;
        } else {
            activityCount++;
            weeklyDistance += distance;
        }

        // BUG: seems like duration data sent by the garmin API is not correct
        //      => https://forums.garmin.com/developer/connect-iq/i/bug-reports/bug-userarctivity-class-member-duration-returns-incorrect-type
        // weeklyDuration += duration.toNumber();
        System.println("[NFO] ActivityStat.addActivityData() - count: " + activityCount + " - wkDistance: " + weeklyDistance);
    }

    public function getStorageData() as Dictionary<Number or String, Number or String or Null> {
        var statDict = {};
        statDict.put("sport"             , activity);
        statDict.put("lastStart"         , lastStart);
        statDict.put("activityCount"     , activityCount);
        statDict.put("weeklyDistance"    , weeklyDistance);
        statDict.put("weeklyDuration"    , weeklyDuration);
        statDict.put("prevActivityCount" , prevActivityCount);
        statDict.put("prevWeeklyDistance", prevWeeklyDistance);
        statDict.put("prevWeeklyDuration", prevWeeklyDuration);

        return statDict;
    }

    public function restoreData(data as Dictionary<Number or String, Number or String or Null>) {
        activity            = data["sport"];
        lastStart           = data["lastStart"];
        activityCount       = data["activityCount"];
        weeklyDistance      = data["weeklyDistance"];
        weeklyDuration      = data["weeklyDuration"];
        prevActivityCount   = data["prevActivityCount"];
        prevWeeklyDistance  = data["prevWeeklyDistance"];
        prevWeeklyDuration  = data["prevWeeklyDuration"];
        System.println("[NFO] ActivityStat.restoreData() - sport: " + activity + " - count: " + activityCount + " - wkDistance: " + weeklyDistance);
    }

    public function newWeekStarts() {
        prevActivityCount  = activityCount;
        prevWeeklyDistance = weeklyDistance;
        prevWeeklyDuration = weeklyDuration;
        activityCount      = 0;
        weeklyDistance     = 0;
        weeklyDuration     = 0;
    }

    var activity           as Lang.Number = 0;
    var lastStart          as Lang.Number = 0;

    var activityCount      as Lang.Number = 0;
    var weeklyDistance     as Lang.Number = 0;
    var weeklyDuration     as Lang.Number = 0;

    var prevActivityCount  as Lang.Number = 0;
    var prevWeeklyDistance as Lang.Number = 0;
    var prevWeeklyDuration as Lang.Number = 0;
}

var wfData as Dictionary<Lang.String, Lang.Number> = {};
// var sports as Array = [null, null, null];
var sports as Array<Lang.Number> = [];
var statistics as Dictionary<Number, ActivityStat or Null> = {};

(:background)
class MyTriathlonWatchface extends Application.AppBase {
//    var serviceDelegate = new MyServiceDelegate();
    var inBackground=false;
    var watchView = null;

    var cmplHR    = null;
    var cmplSteps = null;

    //! Constructor
    // INFO: called for each background event! why ?? no idea
    //     refer to https://forums.garmin.com/developer/connect-iq/f/discussion/289645/initialize-function-of-a-background-service-runs-every-time-a-ontemporalevent-within-the-same-class-is-ran
    public function initialize() {
        AppBase.initialize();
    }


    function saveState() {
        System.println("[NFO] AnalogWatch.saveState()" );

        // First clear everything
        Storage.clearValues();

        // Then save data
        Storage.setValue("wfData", $.wfData );
        Storage.setValue("sports", $.sports );

        var s = 0;
        while (s<$.sports.size()) {
            if ($.sports[s] != null) {
                var data = $.statistics[sports[s]];
                if (data!=null) {
                    Storage.setValue("sport"+(s+1), data.getStorageData());
                    System.println("[NFO] AnalogWatch.saveState(): Saved sport"+(s+1));
                } else {
                    System.println("[WRN] AnalogWatch.saveState(): Nothing to save for sport" + (s+1) + ": " + $.sports[s] );
                }
            } else {
                System.println("[WRN] AnalogWatch.saveState(): sport" + (s+1) + " is not defined");
            }
            s++;
        }
        if (s==0) {
            System.println("[WRN] AnalogWatch.saveState(): no sport saved");
        }
    }

    function restoreState() as Void {
        System.println("[NFO] AnalogWatch.restoreState()" );

        var tmpWfData = Storage.getValue("wfData") as Dictionary<Lang.String, Lang.Number>;
//        var tmpSports = Storage.getValue("sports");

//        if (tmpSports==null) {
            $.sports = [Activity.SPORT_SWIMMING, Activity.SPORT_CYCLING, Activity.SPORT_RUNNING];
            Storage.setValue("sports", $.sports);
//        } else {
//            $.sports = tmpSports;
//        }

        if (tmpWfData == null) {
            // No data has ever been saved, let's initialize from scratch and save
            System.println("[WRN] AnalogWatch.restoreState(): missing data during restore -> let's update all stats");
            updateStats();
            saveState();
            return;
        }

        var match = true;
        var tmpSport = null;
        var tmpStats = {};
        var s = 0 as Lang.Number;
        while (s<$.sports.size()) {
            var data = Storage.getValue("sport"+(s+1)) as Dictionary<Number or String, Number or String or Null>;
            if (data!=null) {
                if (data["sport"] == $.sports[s]) {
                    tmpSport = new ActivityStat(data);
                    tmpStats.put(tmpSport.activity, tmpSport);
                } else {
                    System.println("[WRN] AnalogWatch.restoreState(): unmatched activity: " + data["sport"]);
                    match = false;
                }
            }
            s++;
        }

        if (match==true) {
            $.statistics = tmpStats;
            $.wfData     = tmpWfData;
            System.println("[NFO] AnalogWatch.restoreState(): data restored, lastUpdate: " + new Time.Moment(wfData["lastUpdate"]) );
        } else {
            // No match => Recalculate stats
            System.println("[WRN] AnalogWatch.restoreState(): activity match problem detected during restore -> let's update all stats");
            updateStats();
            saveState();
        }

        return;
    }

    //! Handle app startup
    //! @param state Startup arguments
    public function onStart(state as Dictionary?) as Void {
        System.println("[NFO] AnalogWatch.onStart() - states=" + state + " - inBackground=" + inBackground );
    }


    //! Handle app shutdown
    //! @param state Shutdown arguments
    public function onStop(state as Dictionary?) as Void {
        System.println("[NFO] AnalogWatch.onStop() - inBackground=" + inBackground );
        if (!inBackground) {
            //Background.deleteTemporalEvent();
        }
    }

    public function getServiceDelegate() as Array<ServiceDelegate> {
        System.println("[NFO] AnalogWatch.getServiceDelegate()");
        inBackground=true;
        return [new $.MyServiceDelegate()];
    }

    function registerForNextWeek() {
        // Determine date/time of last monday
        var today  = Gregorian.info(Time.now(), Time.FORMAT_SHORT);
        var addDay = 1;
        if (today.day_of_week != 1) {
            addDay = 9 - today.day_of_week;
        }
        var addDuration = new Time.Duration( addDay * Gregorian.SECONDS_PER_DAY);
        var weeklyResetTime = Time.today().add(addDuration);
        var weekStartInfo = Gregorian.info(weeklyResetTime, Time.FORMAT_SHORT);

        System.println(Lang.format("[NFO] Statistics counters will be reset on $1$-$2$-$3$ $4$:$5$:$6$ (day of week: $7$ - Note: 2 is monday)", [
            weekStartInfo.year.format("%04u"),
            weekStartInfo.month.format("%02u"),
            weekStartInfo.day.format("%02u"),
            weekStartInfo.hour.format("%02u"),
            weekStartInfo.min.format("%02u"),
            weekStartInfo.sec.format("%02u"),
            weekStartInfo.day_of_week
        ]));

        Background.registerForTemporalEvent(weeklyResetTime);
    }

    /*
     * Returns true if statistics have changed.
     */
    function updateStats() as Boolean {
        System.println("[NFO] AnalogWatch.updateStats()");

        var newStats  = {};
        var countAll  = 0;
        var countWeek = 0;
        var countPrevWeek = 0;
        var ua = null;

        var updateStartTime = Time.now();
    
        var userActivityIterator = UserProfile.getUserActivityHistory();
        if (userActivityIterator==null) {
            System.println("[WRN] AnalogWatch.updateStats(): No activity found");
            return false;
        }

        // Determine date/time of last monday
        var today  = Gregorian.info(Time.now(), Time.FORMAT_SHORT);
        var removeDay = 6;
        if (today.day_of_week != 1) {
            removeDay = today.day_of_week - 2;
        }
        var removeDuration = new Time.Duration( removeDay * Gregorian.SECONDS_PER_DAY);
        var weekStart      = Time.today().subtract(removeDuration);

        removeDuration     = new Time.Duration (7 * Gregorian.SECONDS_PER_DAY);
        var prevWeekStart  = weekStart.subtract(removeDuration);

        var weekStartInfo  = Gregorian.info(weekStart, Time.FORMAT_SHORT);
        System.println(Lang.format("[NFO] AnalogWatch.updateStats(): weekStartInfo: $7$ $1$-$2$-$3$ $4$:$5$:$6$", [
            weekStartInfo.year.format("%04u"),
            weekStartInfo.month.format("%02u"),
            weekStartInfo.day.format("%02u"),
            weekStartInfo.hour.format("%02u"),
            weekStartInfo.min.format("%02u"),
            weekStartInfo.sec.format("%02u"),
            weekStartInfo.day_of_week,
        ]));

        // Restore the sports list
        $.sports = Storage.getValue("sports");

        ua = userActivityIterator.next();
        while (ua!=null) {
            countAll++;
            if (ua.startTime!=null) {
//                var info = Gregorian.info(ua.startTime, Time.FORMAT_SHORT);
                if (ua.startTime.lessThan(prevWeekStart)) {
/*
                    System.println(Lang.format("[NFO] " + countWeek + "/" + countAll + ") SKIP Activity: $7$ $1$-$2$-$3$ $4$:$5$:$6$ - sport=$8$", [
                        info.year.format("%04u"),
                        info.month.format("%02u"),
                        info.day.format("%02u"),
                        info.hour.format("%02u"),
                        info.min.format("%02u"),
                        info.sec.format("%02u"),
                        info.day_of_week,
                        ua.type,
                    ]));
*/
                } else if (ua.startTime.lessThan(weekStart)) {
                    countPrevWeek++;
                    var stats=newStats[ua.type];
                    if (stats==null) {
                        stats = new ActivityStat(ua.type);
                    }
                    stats.addActivityData(true, ua.startTime.value(), ua.distance, ua.duration);
                    newStats.put(ua.type,stats);
                } else {
                    countWeek++;
/*
                    System.println(Lang.format("[NFO] " + countWeek + "/" + countAll + ") Activity info: $7$ $1$-$2$-$3$ $4$:$5$:$6$ - sport=$8$ - distance=$9$km - duration=" + ua.duration, [
                        info.year.format("%04u"),
                        info.month.format("%02u"),
                        info.day.format("%02u"),
                        info.hour.format("%02u"),
                        info.min.format("%02u"),
                        info.sec.format("%02u"),
                        info.day_of_week,
                        ua.type,
                        (ua.distance / 1000.0f)
                    ]));
*/
                    var stats=newStats[ua.type];
                    if (stats==null) {
                        stats = new ActivityStat(ua.type);
                    }
                    stats.addActivityData(false, ua.startTime.value(), ua.distance, ua.duration);
                    newStats.put(ua.type,stats);
                }
            } else {
                System.println("[WRN] AnalogWatch.updateStats(): Skipping activity #" + countAll + " with null time");
            }
            ua = userActivityIterator.next();
        }

        System.println("[NFO] AnalogWatch.updateStats(): found " + countWeek + " activities this week, out of " + countAll);

        // Update current statistics
        $.statistics = newStats;
        $.wfData.put("lastUpdate", Time.now().value());

        var updateEndTime = Time.now();
        var updateDuration = updateEndTime.subtract(updateStartTime);
        System.println("[NFO] AnalogWatch.updateStats(): Update duration: " + updateDuration.value() + " seconds" );
        return true;
    }

    function onBackgroundData(data) {
        System.println("[NFO] AnalogWatch.onBackgroundData()");

        if (data==null) {
            return;
        }

        if(data instanceof Number) {
            var n = data as Number;
            if (n == MyServiceDelegate.NFO_ACTIVITY_COMPLETED) {
                if (updateStats()==true) {
                    saveState();
                }
            } else if (n == MyServiceDelegate.NFO_NEW_WEEK) {
                System.println("[NFO] AnalogWatch.onBackgroundData(): received new week event -> reset stats");
                registerForNextWeek();
//                $.statistics = {};
                var s = 0 as Lang.Number;
                while (s<$.sports.size()) {
                    var stat = $.statistics[$.sports[s]];
                    if (stat!=null) {
                        stat.newWeekStarts();
                    }
                    s++;
                }

                saveState();
            } else {
                System.println("[NFO] onBackgroundData(): No data received (status: " + n + ")");
            }
        } 
    }

    // fetches the complication when it changes, and passes to the Watchface
    function onComplicationUpdated(complicationId as Complications.Id) as Void {
        if (watchView != null) {
            try {
                if (complicationId == cmplHR) {
                    var thisComplication = Complications.getComplication(complicationId);
                    if (thisComplication.value != null) {
                        watchView.setHR(thisComplication.value);
                    }
                } else if (complicationId == cmplSteps) {
                    var thisComplication = Complications.getComplication(complicationId);
                    if (thisComplication.value != null) {
                        if (thisComplication.unit==null) {
                            watchView.setSteps(thisComplication.value);
                        } else {
                            // Not null, assuming it's K and that value is now a float...
                            // Refer to https://forums.garmin.com/developer/connect-iq/i/bug-reports/steps-complication-changes-from-number-to-float-1000-when-reaching-10k-steps
                            watchView.setSteps((thisComplication.value*1000).toNumber());
                        }
                    }
                }
            } catch (e) {
                System.println("[ERR] AnalogWatch.onComplicationUpdated(): error passing complication to watchface");
            }
        }
    }

    //! Return the initial view for the app
    //! @return Array Pair [View, Delegate] or Array [View]
    // Note: Bug has been reported about complication storage
    // ==> https://forums.garmin.com/developer/connect-iq/i/bug-reports/not-possible-to-save-complications-id-to-storage-contrary-to-claim-and-example-in-docs-4-2-0-beta-1-sdk
    public function getInitialView() as Array<Views or InputDelegates>? {
        System.println("[NFO] AnalogWatch.getInitialView()");

        restoreState();

        Background.registerForActivityCompletedEvent();
        registerForNextWeek();

        Complications.registerComplicationChangeCallback(self.method(:onComplicationUpdated));
        cmplHR = new Complications.Id(Complications.COMPLICATION_TYPE_HEART_RATE);
//        Storage.setValue("CMPL_HR", cmplHR as PropertyValueType);
        Complications.subscribeToUpdates(cmplHR);

        cmplSteps = new Complications.Id(Complications.COMPLICATION_TYPE_STEPS);
//        Storage.setValue("CMPL_STEPS", cmplSteps as PropertyValueType);
        Complications.subscribeToUpdates(cmplSteps);

        if (WatchUi has :WatchFaceDelegate) {
            watchView = new $.AnalogView();
            var delegate = new $.AnalogDelegate(watchView);
            return [watchView, delegate] as Array<Views or InputDelegates>;
        } else {
            return [new $.AnalogView()] as Array<Views>;
        }
    }

    //! This method runs when a goal is triggered and the goal view is started.
    //! @param goal The goal type that triggered
    //! @return The view to push
    public function getGoalView(goal as GoalType) as Array<View>? {
        return [new $.AnalogGoalView(goal)] as Array<View>;
    }

    //! Return the settings view and delegate
    //! @return Array Pair [View, Delegate]
    public function getSettingsView() as Array<Views or InputDelegates>? {
        return [new $.AnalogSettingsView(), new $.AnalogSettingsDelegate()] as Array<Views or InputDelegates>;
    }
}
